/*global require:true,console:true, process:true*/
try {
  (function() {
    var myfunc = function(){
      function playGame(opt1,opt2) {
        var options = '\n'+opt1+' '+opt2+'\n';
        //both equal, it will be a tie
        if(opt1 === opt2) {
          return (options+'It\'s a tie!!');
        }     
        //if rock vs xx
        if(opt1 === 'rock') {
          if(opt2 === 'paper') {
            return (options+'Paper Wins!!');
          }
          return (options+'Rock Wins!!');
        }
        //if paper vs xx
        if(opt1 === 'paper') {
          if(opt2 === 'rock') {
            return (options+'Paper Wins!!');
          }
          return (options+'Scissor Wins!!');
        }
        //if scissor
        if(opt1 === 'scissor') {
          if(opt2 === 'paper') {
            return (options+'Scissor Wins!!');
          }
          return (options+'Rock Wins!!');
        }
        //nothing matches, might be an error
        return 'OOPS!!..Invalid Data found..!!';
      }
      //main process..
      var spawn = require('child_process').spawn;
      var child1 = spawn('node',['child1.js']);
      child1.stdin.write('choice1');
      //child2.stdout.write('choice2');
      child1.stdout.once('data',function(ch1) {
        ch1 = ch1.toString();
        //create one more process to get second choice..
        var child2 = spawn('node',['child2.js']);
        child2.stdin.write('choice2');
        //get choice 2 and do processing
        child2.stdout.once('data',function(ch2){
          ch2 = ch2.toString();
          console.log(playGame(ch1,ch2));
          //restart the process..
          myfunc();
        });
        child2.stderr.on('data',function(err){
          throw err;
        });
      });
      child1.stderr.once('data',function(err) {
        throw err;
      });
    };
    //initiate
    myfunc();
  })();
} catch(e) {
  console.log('\nerr: '+e);
}