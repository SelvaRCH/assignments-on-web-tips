/*global console:true, module:true, require:true, process:true*/
"use strict ";
(function(){
  console.log("\n");
  //In closure, we can pass any value to outer function and update in inner function..
  //But we could not access the memory of closure function., because scope will not be defined (or initialized) while accessing outside the closure.. 
  function test () {
    (function(){
      var temp;
      console.log(temp);
    })();
  }
  test(); //prints undefined
//  temp = 50;
  test(); //prints undefined


  //'temp = 50;' will create a object to global object(current scope).
  //current scope for closure will be initialized only on function call
  console.log("\nMemory of closure could not be modified..");
})();