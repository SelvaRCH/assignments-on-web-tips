/*global console:true, module:true, require:true, process:true*/
"use strict ";
(function() {
  try {
    //require dependencies
    var rl = require('readline-sync');
    var isEmptyString = function(data) {
      if(data.trim()) {
        return false;
      }
      return true;
    };
    //define arithmetic func
      var arithmetic = function(op1,op2) {
        var a = op1;
        var b = op2;
        var add = (function() {
          return a+b;
        })();
        var subtract = (function() {
          return a-b;
        })();
        var multiply = (function() {
          return a*b;
        })();
        var divide = (function() {
          return a/b;
        })();
        //dispplay to the console
        console.log("Add: "+add+"\nSubtract: "+subtract+"\nMultiply :"+multiply+"\nDivide :"+divide);
      };
      var op1,op2;
      console.log("\n..Enter two values to perform arithmetic operations..(Using Closures)\n");
      //read values and pass it..
      op1 = rl.question("Enter operand 1..  ");
      //validate for empty string..
      if(isEmptyString(op1)) {
        throw "Invalid input..";
      }
      //convert string to numbers..
      op1 = Number(op1); 
      //validate the input adn proceed..
      if(isNaN(op1)) {
        throw "Invalid input..";
      }
      op2 = rl.question("Enter operand 2..  ");
      if(isEmptyString(op2)) {
        throw "Invalid input..";
      }
      //validate..
      if(isEmptyString(op2)) {
        throw "Invalid input..";  
      }
      //convert string to numbers..
      op2 = Number(op2);
      //validate the input adn proceed..
      if(isNaN(op2)) {
        throw "Invalid input..";
      }
      arithmetic(op1,op2);
  } catch(e) {
    console.log("OOPS!! error occured..!! --> "+e);
  }
})();