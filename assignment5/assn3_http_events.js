/*global http:true, require:true, console:true*/
(function(){
  try {
    //require fs, events  and http module
    var fs = require('fs');
    var http = require('http');
    var events = require('events');
    //set the path of download
    //define path
    var url = 'http://nodejs.org/dist/v0.12.4/node.exe';
    //define destination
    var fileLoc = './node.exe';
    //create a stream to write the downloaded data to file
    var file = fs.createWriteStream(fileLoc);
    //necessary storage
    var sizeDownloaded = 0, totalSize, formatMB = 1048576, length, temp=1;
    //create a custom event
    var customEvent = new events.EventEmitter(); 
    //define func to handle the file close
    var handleFileClose = function(err) {
      if(err) {
//        console.log(err);
        //on error, delete the downloaded file..
        fs.unlink(fileLoc);
        throw err;
      }
      //close the file..
      file.close();
      console.log('File downloaded Successfully..');
      return;
    };
    // create a http-request
    http.get(url,function(response) {
      //get the total length and convert to MB
      length = parseInt(response.headers['content-length'],10);
      totalSize = length/formatMB;
      //on callback write the data to file.
      response.pipe(file);
      response.on('data',function(data) {
        //notify the user for each percent of download
        sizeDownloaded += data.length;
        if(Math.floor((sizeDownloaded/length)*10) === temp) {
          ++temp;
          customEvent.emit('notify','Downloading '+((sizeDownloaded/length)*100.0).toFixed(2)+
                    '%  ('+(sizeDownloaded/formatMB).toFixed(2)+' mb of '+totalSize.toFixed(2)+' mb)');
        }
      });
      response.on('end',function() {
        customEvent.emit('http-end');
      });
    }).on('error',function(err) {
      customEvent.emit('http-error',err);
    });
    //envent listeners
    customEvent.on('http-error',function(err) {
      //if error occurs then close the file and delete
      file.close(handleFileClose(err));
      fs.unlink(fileLoc);
    });
    customEvent.on('http-end',function() {      
      handleFileClose();
    });
    customEvent.on('notify',function(notification) {
      console.log(notification);
    });
  } catch(e) {
    console.log(e);
  }
})();