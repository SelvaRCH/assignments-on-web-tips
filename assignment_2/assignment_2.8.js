/*global console:true, module:true, require:true, process:true*/
"use strict ";
(function() {
  try {
    //require dependencies
    var rl = require('readline-sync');
    var isEmptyString = function(data) {
      if(data.trim()) {
        return false;
      }
      return true;
    };
    var arithmetic = {
      a: 0,
      b: 0,
      calculate: function(a,b) {
        // no work around needed, this will refer to the because object passed in apply
        this.a = a;  //this refers object arithmetic, but inside function it will bound to global object
        this.b = b;
        function add() {
          console.log("add: "+(this.a+this.b));
        }
        function subtract() {
          console.log("subtract: "+(this.a-this.b));
        }
        function multiply() {
          console.log("multiply: "+(this.a*this.b));
        }
        function divide() {
          console.log("divide: "+(this.a/this.b));
        }
        //function invocation..
        add.apply(arithmetic); subtract.apply(arithmetic); multiply.apply(arithmetic); divide.apply(arithmetic);
      }
    };
    var op1,op2;
    console.log("\n..Enter two values to perform arithmetic operations..(Apply invocation)\n");
    //read values and pass it..
    op1 = rl.question("Enter operand 1..  ");
    //validate for empty string..
    if(isEmptyString(op1)) {
      throw "Invalid input..";
    }
    //convert string to numbers..
    op1 = Number(op1); 
    //validate the input adn proceed..
    if(isNaN(op1)) {
      throw "Invalid input..";
    }
    op2 = rl.question("Enter operand 2..  ");
    if(isEmptyString(op2)) {
      throw "Invalid input..";
    }
    //validate..
    if(isEmptyString(op2)) {
      throw "Invalid input..";  
    }
    //convert string to numbers..
    op2 = Number(op2);
    //validate the input adn proceed..
    if(isNaN(op2)) {
      throw "Invalid input..";
    }
    //pass the argument to arithmetic functions
    arithmetic.calculate(op1,op2);  //method invocation
  } catch(e) {
    console.log("OOPS!! error occured..!! --> "+e);
  }
})();