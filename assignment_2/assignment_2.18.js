/*global console:true, module:true, require:true, process:true*/
"use strict ";
var MYAPP218 = {};
MYAPP218.revStr = function(str) {
  if(typeof str === 'string') {  
    return (str.split('').reverse().join(''));
  }
  return ("Found type : "+typeof str);
};
//create a string..
var str = "This is TIPS 2015";
//display the reversed string..
console.log("Reversed String.. : "+MYAPP218.revStr(str));
////////////////////////////////////////////////////////////////////////////
module.exports = function() {
  return {
    revStr: MYAPP218.revStr
  };
};
