/*global process:true, console:true*/ /*jshint -W053*/
try {
  // first child process to provide input 1
  //resume the process
  process.stdin.resume();
  //get the input from main process
  process.stdin.on('data',function(data) {
    try {
      data = data.toString();
      if(data != 'choice1') {
        throw 'invalid process prompting for service..';
      }
//      define choice array
      var myArr = ['rock','paper','scissor'];
      //generate a random number bw 0-2
      var random = Math.floor(Math.random()*3);
//      return the choice
      var string  = myArr[random];
      process.stdout.write(string);
    } catch(e) {
      process.stderr.write(e.toString());
    }
  });
} catch(e) {
  process.stderr.write(e.message);
}