/*global console:true, module:true, require:true, process:true*/
"use strict ";
//require library
var rl = require('readline-sync');
//require library
var rlasyncTemp = require('readline');
//creating interface to interact through console
var rlasync = rlasyncTemp.createInterface({
  input: process.stdin,
  output: process.stdout
});
var MYAPP24 = {};
var solitonTrainingRoom = {};
MYAPP24.addKeyValue = function(key,value,solitonTrainingRoom) {
  if(!(key && value)) {
    return solitonTrainingRoom;
  }
  if(typeof solitonTrainingRoom === 'object') {
    //add dynamically to obj solitontrainingroom
    solitonTrainingRoom[key] = value;
    console.log("\nAdded successfully in solitonTrainingRoom\n");
  }
  return solitonTrainingRoom;
};
//check if object is empty
MYAPP24.isEmptyObject = function(obj) {
    if(typeof obj === 'object' && Object.keys(obj).length)
    {
      return false;
    }
    return true;
};
var initiateProcess = function() {
  //declare an object..
  var addDynamic = {};
  var myRoute = function(func) {
    rl.question("Please press any key to continue \n");
    func();
  };  
  var getRequired = function(item) {
    var valueRead = rl.question("Please enter a "+item+"..  ");
    if(valueRead) {
      return valueRead;
    } else {
      console.log("\nFound Empty "+item+"  ..Exiting..\n");
      myRoute(options);
    }
  };
  var DisplayProp = function() {
    if(!MYAPP24.isEmptyObject(solitonTrainingRoom)) {
      console.log("----------------------------------------------------------------------------");
      for(var prop in solitonTrainingRoom) {
        //iterate over prop of current obj, and not its prototypes
        //also check for type number
        if(solitonTrainingRoom.hasOwnProperty(prop)) {
          console.log(prop+": "+solitonTrainingRoom[prop]);
        }
      }
      console.log("----------------------------------------------------------------------------");
    } else {
      console.log("\nNo Record Found\n");
    }
    myRoute(options);
  };
  var options = function() {
    //print avalable opt
    var optionList = "\nPlease choose an option below..";
    optionList += "\n1.Add key-value pair\n2.View all prop in solitonTrainigRoom\n3.Exit\n";
    optionList += "Enter Here.. ";
    rlasync.question(optionList,function(choice){
      choice = Number(choice);
      if(choice) {
        switch(choice) {
          case 1: 
            //read the input from user
            addDynamic.key = getRequired("Key");
            addDynamic.value = getRequired("Value");
            solitonTrainingRoom = MYAPP24.addKeyValue(addDynamic.key,addDynamic.value,solitonTrainingRoom);
            myRoute(options);
            break;
          case 2: DisplayProp(); break;
          case 3: console.log("..Thank You..");  process.exit(0); break;
          default : console.log("Sorry invalid choice.."); options();
        }
      } else {
        console.log("Found inappropriate value.."); options();
      }
    });
  };  
    //start adding key-value pair
    options();
};
//initiate the process
initiateProcess();
//////////////////////////////////////////////////////////////
module.exports = function() {
  return {
    isEmptyObject: MYAPP24.isEmptyObject,
    addKeyValue: MYAPP24.addKeyValue
  };
};