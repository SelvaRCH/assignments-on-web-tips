/*global console:true, module:true, require:true, process:true*/
"use strict ";
//require library
var rlasyncTemp = require('readline');
var rl = require('readline-sync');
//creating interface to interact through console
var rlasync = rlasyncTemp.createInterface({
  input: process.stdin,
  output: process.stdout
});
var MYAPP219 = {};
(function(){
  //create storage
  var stack = [];
  //route to desired func
  function myRoute (func) {
    rl.question("\nPlease enter any key to continue..  ");
    func();
  }
  //push an element
  MYAPP219.pushElem = function(ele,stack) {
    if(!(ele && Array.isArray(stack))) {
      console.log("\nFound invalid value.. Exiting..");
      return false;
    } else {
      stack.unshift(ele);
    }
    return stack;
  };
  MYAPP219.popElem = function(stack) {
    if(!(Array.isArray(stack) && stack.length)) {
      console.log("\nEmpty Stack.. Exiting..");
      return false;
    } else {
      stack.shift();
      console.log("Element popped.."); 
    }
    return stack;
  };
  function dispStack (stack) {
    if(!stack.length) {
      console.log("\nEmpty Stack Found..");
    } else {
      for(var i=0; i<stack.length; ++i) {
        console.log(stack[i]);
      }
    }
  }
  function startProcess() {
    var welcomeList = "\nPlease enter any option from listed..";
    welcomeList += "\n1.Push\n2.Pop\n3.View\n4.Exit\n\nEnter your choice here..  ";
    rlasync.question(welcomeList,function(choice){      
    switch(choice) {
      case '1':        
        var ele  = rl.question("Please provide a value to push..  ");
        MYAPP219.pushElem(stack);
        break;
      case '2':
        MYAPP219.popElem(stack);
        break;
      case '3':
        dispStack(stack);
        break;
      case '4':
        console.log("\n..Thank You..");
        process.exit(0);
        break;
      default : console.log("Invalid Choice Try again.."); myRoute(startProcess);
    }      
    myRoute(startProcess);
    });
  }
  //initiate the process..
  startProcess();
})();
////////////////////////////////////////////////////////////////////////////
module.exports = function() {
  return {
    pushElem: MYAPP219.pushElem,
    popElem: MYAPP219.popElem
  };
};
