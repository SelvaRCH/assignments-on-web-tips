/* global require:true, console: true, process: true, module:true*/
//require library
var readline = require('readline');
//creating interface to interact through console
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
//route function
var routeDesired = function(func) {
  rl.question("Please enter any key to continue..\n",function(){
    //call the specific function ..
    func();
  });
};
var startCommand = function() {
  //define the function to print diamond
  var printDiamond = function () {
    //declare necessary storage
    var length,pos,i,j,width,singleLine='';
    //read the length from console..
    rl.question("Please define the length of diamond....  ", function(data) {
      //get a number from data that read.
      length = parseInt(data);
      //find maximum width for length
      width = (2*length)-1;
      //find the position of initial occurance of star
      pos = length;
      //start building by validating the length
      if(isNaN(length)) {
        console.log("\nsorry we can't find a valid length.. Good Luck Next Time\n");
        //route to beginning
        routeDesired(printDiamond);
//        start the process from first..
      } else {
        console.log("choice identified -> "+length);
        //logic to print diamond
        for(i=1; i<=(2*length)-1; ++i) {
          singleLine = '';
          for(j=1; j<=width; j++) {
            //add star from pos
            if(pos === j || (width-j+1) === pos) {
              singleLine += '*';
            } else {
              singleLine += ' ';
            }
          }
          if(i<length) {
            --pos;
          } else {
            ++pos;
          }
          console.log(singleLine);
        }
        //route to beginning
        routeDesired(startCommand);
      }
    });
  };
  //define the function to print Fibonocci
  var printFibonocci = function() {
    //declare necessary storage
    var length, temp1 = 0, temp2 = 1, sum, stringFibo, i;
    //read the length from console
    rl.question("Please define length for fibonocci series....  ",function(data){
      length = parseInt(data);
      //start building by validating the length
      if(isNaN(length) || length <= 0) {
        console.log("\nsorry, we can't find a valid length.. Good Luck Next Time\n");
        //route to beginning
        routeDesired(printFibonocci);
      } else {
        console.log("choice identified -> "+length);
        stringFibo = '';
        if(length === 1) {
          stringFibo += (temp1+' ');
        } else {
          stringFibo += (temp1+' '+temp2+' ');
          for(i=3; i<=length; ++i) {
            sum = temp1+temp2;
            stringFibo += sum+' ';
            temp1 = temp2;
            temp2 = sum;
          }
        }
        console.log("Fibo series: "+stringFibo);
        //route to beginning
        routeDesired(startCommand);
      }
    });
  };
  //define the function to print reversed string
  var reverseString = function() {
    //declare necessary storage
    var reversedString = '',i;
    //initialize the constraints
    var constraints = ['a','e','i','o','u',' '];
    var content = "Note: This will delete the vowels and whitespaces from the string..\n";
    content += "Please enter the String to get the reversed result..\n";
    //read the length from console
    rl.question(content,function(data) {
      //start building by validating the length
      if(data && data.length) {
        //iterate through the string in reverse
        for(i=data.length-1; i>=0; --i) {
          //if element of data is not found in constraints, add it.
          if(constraints.indexOf(data[i].toLowerCase()) === -1) {
            reversedString += data[i];
          }
        }
        //print the result in console
        console.log("Reversed: "+reversedString);
        //route to beginning
        routeDesired(startCommand);
      } else {
        console.log("Sorry, we can't find a valid string.. Empty String Found\n");
        //route to beginning
        routeDesired(reverseString);
      }
    });
  };
  //sort the stuudent list by Id
  var sortList = function() {
    //create an array of student list that holds id, name, age, major subject
    //declare necessary storage
    var studentList = [
      {
        "id": 46,
        "name": "Selvaraj",
        "age": 22,
        "major": "ECE"
      },
      {
        "id": 45,
        "name": "Sanjiv",
        "age": 22,
        "major": "CSE"
      },
      {
        "id": 44,
        "name": "Kaushik",
        "age": 22,
        "major": "IT"
      },
      {
        "id": 47,
        "name": "Subash",
        "age": 22,
        "major": "ECE"
      },
      {
        "id": 42,
        "name": "Surya Prakash",
        "age": 22,
        "major": "CSE"
      }
    ];
    var sortbyId = function(a,b) {
      return a.id - b.id;
    };
    var displayList = function(list) {
      var studentStr = '';
      for (var el in list) {
        studentStr += "Id: " +list[el].id + ", "  + list[el].name+" from " + list[el].major;
        studentStr += " is "+ list[el].age + "\n";
      }
      //display the list
      console.log(studentStr);
    };
    //display the  unsorted student list
    console.log("\nBEFRE SORT..");
    displayList(studentList);
    rl.question("Please ENTER a key to view the list, sorted by ID..  ",function(data){
      console.log("\nAFTER SORT..");
      displayList(studentList.sort(sortbyId));
      //route to beginning
        routeDesired(startCommand);
    });
  };
  //add books and do search
  var stuffStore = function() {
    //
    console.log("\nWelcome to book store..");
    //set a factory to provide info
    function stuffFactory () {
      var temp = [
        {
          "author": "Douglas Crockford",
          "books": "Javascript the good parts"
        },
        {
          "author": "David Flanagan",
          "books": "JavaScript, The Definitive Guide"
        }
      ];
      return temp;
    }
    //get the books from factory..
    var collection = stuffFactory();
    //display books avaliable,
    var displayAvailableBooks = function() {
      var disp = '-------------------------------------------------------------------------------';
      for(var el in collection) {
        disp += ("\n\""+ collection[el].books+"\" by, "+collection[el].author);
      }
      disp += '\n-------------------------------------------------------------------------------';
      console.log(disp);
      //route to beginning
        routeDesired(optionsHome);
    };
    //function to add books
    var addStuffToStore = function() {
      try {
        //declare necessary storage
        var localDetailStorage = {};
        //read book name
        rl.question("Please enter a book name..",function(bookName) {
          if(!bookName.trim()) {
            console.log("Empty name found..!!");
            //route to beginning
            routeDesired(optionsHome);
          }
          localDetailStorage.books= bookName;
          rl.question("Please enter author name..",function(authorName) {
            if(!authorName.trim()) {
              console.log("Empty name found..!!");
              //route to beginning
              routeDesired(optionsHome);
            }
            localDetailStorage.author = authorName;
            //push it to array..
            collection.push(localDetailStorage);
            console.log("..books added..");
        //route to beginning
          routeDesired(optionsHome);
          });
        });
      } catch(err) {
        console.log("error occurred: "+err);
      }
    };
    //search books..
    var searchBooks = function() {
      try {
        var temp = 0;
        //get the input..
        rl.question("Enter key word to search..",function(searchText) {
          //throw error if name is empty..
          if(!searchText.trim()) {
            console.log("Found Empty Pattern!!");
            //route to beginning
            routeDesired(optionsHome);
          } else {
            //find books and display
            for(var el in collection) {
              //if book matches w ith pattern then display
              if(collection[el].books.toLowerCase().indexOf(searchText) !== -1) {
                if(!temp) {
                  console.log("\n-------------------------------------------------------------------------------");
                }
                ++temp;
                console.log("\nFound: \""+collection[el].books+"\""+" by, "+collection[el].author);
              }
            }
            if(!temp) {
              console.log("\nNo Books matches with the specified pattern..!!\n");
            } else {
              console.log("\n-------------------------------------------------------------------------------\n");
            }
            //route to beginning
            routeDesired(optionsHome);
          }
        });
      } catch(err) {
        console.log("Error Occurred.. err: "+err+"\n");
        routeDesired(searchBooks);
      }
    };
    //declare necessary storage..
    var routeForStuffStore;
    var optionsHome = function() {
      var displayQuestion = "\nPlease enter your choice from options listed..";
      displayQuestion += "\n\n1.View available books\n2.Add Books to Stuff store\n3.Search Books..\n4.Exit from Stuff Store\n";
      rl.question(displayQuestion+"\nEnter here..  ",function(choice){
        routeForStuffStore = Number(choice);
        if(isNaN(routeForStuffStore)) {
          console.log("\nSorry.. Please enter number.. Exiting..");
        //route to beginning
        routeDesired(optionsHome);
        } else {
          switch(routeForStuffStore) {
            case 1: displayAvailableBooks(); break;
            case 2: addStuffToStore(); break;
            case 3: searchBooks(); break;
            case 4: startCommand(); break;
            default: console.log("\nSorry.. Invalid choice.."); optionsHome();
          }
        }
        });
    };
    //route to beginning
    optionsHome();
  };
  //read the choice and exec appropriate func
  var initialData = "\nHave a look on the choices below";
  initialData += "\n\n1.Print Diamond\n2.Create Fibonocci Series";
  initialData += "\n3.Reverse a string\n4.Sort the student list by Id\n5.Stuff Store\n6.Exit\n";
  initialData += "Enter your choice in number..";
  rl.question(initialData, function(choice) {
    choice = parseInt(choice);
    switch(choice) {
      case 1: printDiamond(); break;
      case 2: printFibonocci(); break;
      case 3: reverseString(); break;
      case 4: sortList(); break;
      case 5: stuffStore(); break;
      case 6:
        console.log("..Thank You..");
        //close interface at the end of program and exit
        rl.close(); process.exit(); break;
      default: 
        console.log("\nSorry.. Invalid Choice..Try again\n");
      //route to beginning
      routeDesired(startCommand);
    }
  });
};
  //trigger the function to start from first..
  console.log("\n---------------------------Assignment------------------------------------------");
/////////////////////////////////////////////////////////////////////////////////////////////
//initially trigger the process..
startCommand();
/////////////////////////////////////////////////////////////////////////////////////////////