/*global http:true, require:true, console:true*/
(function(){
  try {
    //require fs, q  and http module
    var fs = require('fs');
    var q = require('q');
    var http = require('http');
    //set the path of download
    //define path
    var url = 'http://nodejs.org/dist/v0.12.4/node.exe';
    //define destination
    var fileLoc = 'node.exe';
    //create a stream to write the downloaded data to file
    var file = fs.createWriteStream(fileLoc);
    //necessary storage
    var sizeDownloaded = 0, totalSize, formatMB = 1048576, length, temp=1;
    //define func to handle the file close
    var handleFileClose = function(err) {
      if(err) {
        console.log(err);
        //on error, delete the downloaded file..
        fs.unlink(fileLoc);
        throw err;
      }
      //close the file..
      file.close();
      console.log('File downloaded Successfully..');
      return;
    };
    // create a http-request
    var myHttpDownload = function(url) {
      //create a promise
      var defered = q.defer();
      http.get(url,function(response) {
        //get the total length and convert to MB
        length = parseInt(response.headers['content-length'],10);
        totalSize = length/formatMB;
        //on callback write the data to file.
        response.pipe(file);
        response.on('data',function(data) {
          //notify the user for each percent of download
          sizeDownloaded += data.length;
          if(Math.floor((sizeDownloaded/length)*10) === temp) {
            ++temp;
            defered.notify('Downloading '+((sizeDownloaded/length)*100.0).toFixed(2)+
                      '%  ('+(sizeDownloaded/formatMB).toFixed(2)+' mb of '+totalSize.toFixed(2)+' mb)');
          }
        });
        response.on('end',function() {
          defered.resolve('success');
        });
      }).on('error',function(err) {//error callback
        defered.reject(err);
      });
      return defered.promise;
    };
    //call the function
    myHttpDownload(url).then(function(sucsCB) {
      //on success, close the handle and exit
      handleFileClose();
    },function(errCB) {
      //if error occurs then close the file and delete
      file.close(handleFileClose(errCB));
      fs.unlink(fileLoc);
    },function(notification) {
      //display the notification
      console.log(notification);
    });
  } catch(e) {
    console.log(e);
  }
})();