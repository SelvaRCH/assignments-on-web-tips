/* global require:true, it: true, describe: true*/
"use strict ";
var exp = require("./assignment_1.js");
var handle = exp();
//test the add function
var assert = require('assert');
describe('printDiamond', function(){
    it('should print 2*(n-1) lines for  length n', function(){
      assert.equal(false,handle.diamondCore(0));
      assert.equal(false,handle.diamondCore(-1));
      assert.equal(false,handle.diamondCore(''));
      assert.equal(false,handle.diamondCore('str'));
      assert.equal('*\n',handle.diamondCore('1'));
      assert.equal('*\n',handle.diamondCore(1));
      assert.equal(' * \n* *\n * \n',handle.diamondCore(2));
    });
});
describe('FIBONOCCI', function(){
    it('should print the fibo series for valid length', function(){
      assert.equal(false,handle.fiboCore(0));
      assert.equal(false,handle.fiboCore(-1));
      assert.equal(false,handle.fiboCore(''));
      assert.equal(false,handle.fiboCore('str'));
      assert.equal('0 1 ',handle.fiboCore('2'));
      assert.equal('0 1 ',handle.fiboCore(2));
      assert.equal('0 ',handle.fiboCore(1));
    });
});
describe('STRING REVERSE', function(){
    it('should print reversed string for given string', function(){
      assert.equal(false,handle.revCore(0));
      assert.equal(false,handle.revCore(-1));
      assert.equal(false,handle.revCore(''));
      assert.equal('rts',handle.revCore('str'));
      assert.equal('',handle.revCore('aeiou  '));
      assert.equal('',handle.revCore('     '));
      assert.equal(false,handle.revCore(undefined));
    });
});
describe('SORT ARRAY', function(){
  var studentList = [
      {
        "id": 46,
        "name": "Selvaraj",
        "age": 22,
        "major": "ECE"
      },
      {
        "id": 45,
        "name": "Sanjiv",
        "age": 22,
        "major": "CSE"
      },
      {
        "id": 44,
        "name": "Kaushik",
        "age": 22,
        "major": "IT"
      },
      {
        "id": 47,
        "name": "Subash",
        "age": 22,
        "major": "ECE"
      },
      {
        "id": 42,
        "name": "Surya Prakash",
        "age": 22,
        "major": "CSE"
      }
    ];
    it('should sort the given arrray', function(){
      assert.equal(false,handle.sortCore(0));
      assert.equal(false,handle.sortCore(-1));
      assert.equal(false,handle.sortCore(''));
      assert.equal(false,handle.sortCore({}));
      assert.equal(false,handle.sortCore([]));
      assert.equal(42,handle.sortCore(studentList)[0].id);
      assert.equal(47,handle.sortCore(studentList)[4].id);
      assert.equal("Kaushik",handle.sortCore(studentList)[1].name);
    });
});
