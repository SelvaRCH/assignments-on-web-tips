/*global console:true, module:true, require:true, process:true*/
"use strict ";
//require library
var rl = require('readline-sync');
//require library
var rlasyncTemp = require('readline');
//creating interface to interact through console
var rlasync = rlasyncTemp.createInterface({
  input: process.stdin,
  output: process.stdout
});
var MYAPP26 = {};
(function() {
  try {
    var isEmptyString = function(data) {
      if(data.trim()) {
        return false;
      }
      return true;
    };
    MYAPP26.arithmetic = {
      a: 0,
      b: 0,
      calculate: function(a,b) {
        var that = this;  //work around
        this.a = a;  //this refers object arithmetic, but inside function it will bound to global object
        this.b = b;
        function add() {
          return (that.a+that.b);
        }
        function subtract() {
          return (that.a-that.b);
        }
        function multiply() {
          return (that.a*that.b);
        }
        function divide() {
          return (that.a/that.b);
        }
        //function invocation..
        console.log("\nadd: "+add()); 
        console.log("subtract: "+subtract()); 
        console.log("multiply: "+multiply());
        console.log("divide: "+divide());
      }
    };
    var op2;
    console.log("\n..Enter two values to perform arithmetic operations..(Func invocation)\n");
    //read values and pass it..
    rlasync.question("Enter operand 1..  ",function(op1){
       try {
        //validate for empty string..
        if(isEmptyString(op1)) {
          throw "Invalid input..";
        }
        //convert string to numbers..
        op1 = Number(op1); 
        //validate the input adn proceed..
        if(isNaN(op1)) {
          throw "Invalid input..";
        }
        op2 = rl.question("Enter operand 2..  ");
        if(isEmptyString(op2)) {
          throw "Invalid input..";
        }
        //validate..
        if(isEmptyString(op2)) {
          throw "Invalid input..";  
        }
        //convert string to numbers..
        op2 = Number(op2);
        //validate the input adn proceed..
        if(isNaN(op2)) {
          throw "Invalid input..";
        }
        //pass the argument to arithmetic functions
        MYAPP26.arithmetic.calculate(op1,op2);  //method invocation
       } catch (er) {
        console.log(er);
        process.exit(0);
      }
    }); 
  } catch(e) {
    console.log("OOPS!! error occured..!! --> "+e);
  }
})();
//////////////////////////////////////////////////////////////
module.exports = function() {
  return {
    arithmetic: MYAPP26.arithmetic
  };
};