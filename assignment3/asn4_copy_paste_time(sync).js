/*global require:true, console:true, process:true*/
"use strict ";
//get fs module..
var fs = require('fs');
//////////////////////////////////////////////////////////////////////////
try {
  var configLoc = './config.json';
  //necessary var to find time req
  var timeTracker = [], fileLength;
  //function to write the measured time in config.json
  var writeMeasuredTime = function() {
    //read the config file..
    var configData = fs.readFileSync(configLoc,'utf-8');
    if(!configData) {
      console.log('time could not be updated in config file');
      throw 'time could not be updated in config file';
    }
    //convert to array format
    configData = JSON.parse(configData);
    if(!(Array.isArray(configData) && configData.length)) {
      console.log('config file changed!!');
      throw 'config file changed!!';
    }
    //write the timings recorded
    for(var i=0; i<timeTracker.length; ++i) {
      if(timeTracker[i].hasOwnProperty('dateEnd')) {
        configData[i].timeRequired = timeTracker[i].dateEnd;
        console.log(i+' '+configData[i].timeRequired+'ms');
      }
    }
    //now write to the file
//    console.log(configData);
    fs.writeFileSync(configLoc,JSON.stringify(configData),'utf-8');
//    console.log('config file updated');
  };
  //where the execution starts
  var copyPaste = function() {
    //verfify the existence of folder and proceed
    //performs validating the path and invoking func to do copy & paste
    function copyToNewFile(src,dest,el) {
//      console.log(dest);
      var dirPath = dest.substr(0,dest.lastIndexOf('\\'));
//      console.log('dirPath: '+dirPath);
      var callback = fs.existsSync(dirPath);
//      console.log('existnce: '+callback);
      //if directory exists then just write to the destination
      if(callback) {
        startCopy(src,dest,el);
      } else {
        //else create a folder and then do copy
        var makeNewDir = fs.mkdir(dirPath+"\\");
        if(!makeNewDir) {
          console.log('failed to create directory: ');
          throw 'failed to create directory: ';
        }
        startCopy(src,dest,el);
      }
    }
      //core func  that performs copy_paste
    var startCopy = function(src,dest,el) {
      var data = fs.readFileSync(src,'utf-8');
//        console.log('file-read :'+src+'\ndata: '+data);
      if(!data) {
        console.log('err occured in file read');
        throw 'err occured in file read';
      }
        //write the data read to destination
      //will return nothing..
      fs.writeFileSync(dest,data,'utf-8');
      //get time2, and find the time required to copy each file
      timeTracker[el].dateEnd = (new Date().getTime()) - (timeTracker[el].dateStart);            
//            console.log(el+' '+timeTracker[el].dateEnd);
//      console.log('data written successfully from src to a dest');
      //once all file is done call func to mark recorded timings
      if(el+1 === fileLength) { 
        //now call a func to write the time in config.json
        writeMeasuredTime();
      }
    };
    //read the config file and start the process..
    var data = fs.readFileSync(configLoc,'utf-8');
    if(!data) {
      throw 'err occurred on reading file, found null data';
    }
//    console.log(data);
//    if(1) {
//    throw 'w';
//    }
    //convert to array format
    data = JSON.parse(data);
    if(!(Array.isArray(data) && data.length)) {
      console.log('not a valid JSON');
      throw 'not a valid JSON';
    }
    //update the length of data Array
    fileLength = data.length;
    //display the data
//      console.log(data);
    //run a loop open the specified file, and write to destination
    for(var el=0; el<data.length; ++el) {
//        console.log(data[el].source,data[el].destination);
      //get time1, befe reading src file
      timeTracker[el] = {};
      timeTracker[el].dateStart = new Date().getTime();
      copyToNewFile(data[el].source,data[el].destination,el);
    }
  };
  //start the process..
  copyPaste();
} catch(e) {
  console.log('err  : '+e);
} finally {
  //write the timimgs recorded
//  writeMeasuredTime();  
}