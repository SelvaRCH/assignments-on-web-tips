/* global require:true, it: true, describe: true, console:true*/
"use strict ";
var exp23 = require("./assignment_2.1,2.2,2.3.js");
var handle23 = exp23();

var exp24 = require("./assignment_2.4.js");
var handle24 = exp24();

var exp25 = require("./assignment_2.5.js");
var handle25 = exp25();

var exp211 = require("./assignment_2.11.JS");
var handle211 = exp211();

var exp212 = require("./assignment_2.12.,2.13,2.14.js");
var handle212 = exp212();

var exp218 = require("./assignment_2.18.js");
var handle218 = exp218();

var exp219 = require("./assignment_2.19.js");
var handle219 = exp219();
//test the add function
var assert = require('assert');
//define proprties for camera.
  var camera = {
    "focalLengthIn(mm)": 2.4,
    "lensDiaIn(mm)": 30,
    "totalStorageUnit": 2000,
    "unitsOccupied": 2,
    "resolutionIn(MP)": 5,
    "weight": "150 gm",
    "yearReleased": 2,
    "color": "Black",
    "price": 50000,
    "brand": "SONY",
    "series": "DSLR E 45",
    "picStorage": ['pic1','pic2'],
    takePic: function() {
      ++this.unitsOccupied;
      this.picStorage.push("pic"+this.unitsOccupied);
      console.log("The amazing moment is captured lively..");
    },
    wipeStorage: function() {
      this.picStorage = [];
      console.log("Storage is cleaned");
    },
    deleteSelectedPic: function(picId) {
      delete this.picStorage[this.storage.indexOf(picId)];
      console.log("picture "+picId+" is deleted");
    }
  };
  var testObj = {
    'name': 'selva'
  };
describe('Printing prop of camera', function(){
    it('should print the keys for only a valid object', function(){
      assert.equal(false,handle23.enumCam({}));
      assert.equal(0,handle23.enumCam(camera).indexOf('focalLengthIn(mm)'));
      assert.equal('a',handle23.enumCam(testObj).charAt(10));
      assert.equal(false,handle23.enumCamTypeNumber(testObj));
    });
});
describe('testing add keys in dynamic', function(){
    it('should add key-value pair to obj if valid', function(){
      assert.equal(testObj,handle24.addKeyValue('','',testObj));
      assert.equal(false,handle24.isEmptyObject(testObj));
      assert.equal(true,handle24.isEmptyObject({}));
      assert.equal('selvaraj',handle24.addKeyValue('name','selvaraj',testObj).name);
      assert.equal(undefined,handle24.addKeyValue('age','',testObj).age);
    });
});
describe('doing arithmetic for method invocation..', function(){
    it('should return the correct result', function(){
      assert.equal(5,handle25.arithmetic.add(0,5));
      assert.equal(0,handle25.arithmetic.add(0,0));
      assert.equal(0,handle25.arithmetic.add(-5,5));
      assert.equal(0,handle25.arithmetic.subtract(5,5));
      assert.equal(105,handle25.arithmetic.subtract(110,5));
      assert.equal(1,handle25.arithmetic.multiply(1,1));
      assert.equal(0,handle25.arithmetic.multiply(1,0));
      assert.equal(1,handle25.arithmetic.divide(1,1));          
      assert.equal(Infinity,handle25.arithmetic.divide(1,0));          
    });
});
describe('Adding prototypes and display valid array',function(){
  it('Display array elements if valid array passed',function(){
    assert.equal(false,handle212.displayInterested([]));
    assert.equal('\n\n',handle212.displayInterested(['','']));
    assert.equal('me\n',handle212.displayInterested(['me']));
    assert.equal(false,handle212.displayInterested({}));
    assert.equal(false,handle212.displayInterested({name: 'selva'}));
    assert.equal(false,handle212.displayInterested(9));
    assert.equal(false,handle212.displayInterested('selva'));
  });
  it('Access the prototypes added for numbers and string..',function(){
    assert.equal(120,Number(5).fact());
    assert.equal(undefined,Number(0).fact());
    assert.equal(false,"VWL".findVowel());
    assert.equal(17,'hhhhaa'.findVowel().indexOf('a'));
  });
});
describe('Check reverse string..',function(){
  it('should return reverse only if valid',function(){
    assert.equal('avles',handle218.revStr('selva'));
    assert.equal('Found type : object',handle218.revStr([]));
    assert.equal('Found type : number',handle218.revStr(8));
    assert.equal('8',handle218.revStr('8'));
  });
});
describe('Stack',function(){
  it('validating push, pop..',function(){
    assert.deepEqual([1,2],handle219.pushElem(1,[2]));
    assert.deepEqual(false,handle219.pushElem('',[1]));
    assert.deepEqual([3],handle219.pushElem(3,[]));
    assert.deepEqual(false,handle219.pushElem(3,{}));
    assert.deepEqual([2,1,4,5],handle219.popElem([3,2,1,4,5]));
    assert.deepEqual(false,handle219.popElem([]));
    assert.deepEqual([],handle219.popElem([1]));
    assert.deepEqual(false,handle219.popElem({'age':22}));
  });
});
describe('Validating Inheritance..',function(){
  it('Pseudo classical..',function(){
    assert.equal('object',(typeof handle211.pseudo.lineCore(10)));
    assert.equal('object',(typeof handle211.pseudo.sqrCore(10)));
    assert.equal(10,handle211.pseudo.sqrCore(10).length);
    assert.equal(20,handle211.pseudo.rectCore(10,20).width);
    assert.equal(10,handle211.pseudo.rectCore(10,20).height);
    assert.equal(100,handle211.pseudo.rhombusCore(100,20).diagonal1);
    assert.equal(2,handle211.pseudo.rhombusCore(100,2).prototype.diagonal2);
  });
  it('Prototypical',function(){
    assert.equal(false,handle211.proto.lineCore(10,{}));
    assert.equal(false,handle211.proto.lineCore(10,[]));
    assert.equal(10,handle211.proto.lineCore(10,{"age":10}).length);
    assert.equal(10,handle211.proto.lineCore(10,{"length":10}).length);
    assert.equal(false,handle211.proto.sqrCore(10,{}));
    assert.equal(false,handle211.proto.sqrCore(10,[]));
    assert.equal(10,handle211.proto.sqrCore(10,{"age":10}).length);
    assert.equal(10,handle211.proto.sqrCore(10,{"length":10}).length);
    assert.equal(false,handle211.proto.rectCore(10,230,{}));
    assert.equal(10,handle211.proto.rectCore(10,230,{'name':'selva'}).length);
    assert.equal(230,handle211.proto.rectCore(10,230,{'name':'selva'}).height);
    assert.equal(false,handle211.proto.rhombusCore(10,230,{}));
    assert.equal(10,handle211.proto.rhombusCore(10,230,{'name':'selva'}).diagonal1);
    assert.equal(230,handle211.proto.rhombusCore(10,230,{'name':'selva'}).prototype.diagonal2);
  });
  it('Functionall',function(){
    assert.equal(56,handle211.functional.getLengthobj({'length':56}).length);
    assert.equal(undefined,handle211.functional.getLengthobj({'age':22}).length);
    assert.equal(undefined,handle211.functional.getLengthobj({'age':22}).age);
    assert.equal(undefined,handle211.functional.getLengthobj({}).length);
    assert.equal(undefined,handle211.functional.getLengthobj('string').length);
    assert.equal(56,handle211.functional.getTwoDimobj({'height':56}).height);
    assert.equal(23,handle211.functional.getTwoDimobj({'length':56,'width':23}).width);
    assert.equal(undefined,handle211.functional.getTwoDimobj({'age':22}).length);
    assert.equal(undefined,handle211.functional.getTwoDimobj({'age':22}).age);
    assert.equal(undefined,handle211.functional.getTwoDimobj({}).length);
    assert.equal(undefined,handle211.functional.getTwoDimobj('string').length);
    assert.equal(56,handle211.functional.getRhomobj({'diagonal2':56}).diagonal2);
    assert.equal(undefined,handle211.functional.getRhomobj({'age':22}).diagonal2);
    assert.equal(undefined,handle211.functional.getRhomobj({'age':22}).age);
    assert.equal(undefined,handle211.functional.getRhomobj({}).diagonal2);
    assert.equal(undefined,handle211.functional.getRhomobj('string').diagonal2);
  });
});