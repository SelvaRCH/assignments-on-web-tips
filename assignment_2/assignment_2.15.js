/*global console:true, module:true, require:true, process:true*/
"use strict ";
(function(){
  //require node-modules
  var rl = require('readline-sync');
  var myInterest = [
      'Cycling',
      'Programming in C',
      'Riding Bikes',
      'Playing Carrom',
      'Listening Music',
      'Reading Short Stories',
      'Learning new Languages'
    ];
  var display = function(array) {
    for(var el = 0; el<array.length; ++el) {
        console.log(array[el]);
    }
  };
  console.log("-------------------->Array of My interest------------------------");
  //display unmodified arr ay
  display(myInterest);
  var newLen = rl.question("\nDefine your new length for array my interest..  ");
  //length property could be updated by assigning a new value..,
  myInterest.length = newLen;
  //display new array
  display(myInterest);
  console.log("\nLength property can be updated by ourselves, But it is not a good way to define length property manually. It may create a issues if not handled properly and used in correct place.. It's always better to avoid this behaviour..");
})();