/*global require:true, console:true, process:true*/
"use strict ";
//get fs module..
var fs = require('fs');
//////////////////////////////////////////////////////////////////////////
try {
  var startExec = function() {
    //define a delete func to delete the file
    var deleteFile = function(path) {
//      console.log(path);
      fs.unlink(path,function(callback) {
        try {
          if(callback) {
//            console.log(path+' not found');
            throw callback;
          } else {
            console.log(path+' deleted');
          }
        } catch(er) {
          console.log(er);
        }
      });
    };
    //path for config file
    var configPath = './config.json';
    //read the json file and parse
    var data = fs.readFileSync(configPath);
    if(!data) {
      console.log('no data found in config file');
      throw 'no data found in config file';
    }
    //convert to array format
    data = JSON.parse(data);
    if(!(Array.isArray(data) && data.length)) {
      console.log('not a valid JSON');
      throw 'not a valid JSON';
    }
    //run a loop to find the files present in config file and delete
    for(var i=0;i<data.length; ++i) {
      if(data[i].hasOwnProperty('destination')) {
        deleteFile(data[i].destination);
      }
    }
  };
  //start the execution
  startExec();
} catch (e) {
  console.log('err :'+e);
}