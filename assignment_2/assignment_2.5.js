/*global console:true, module:true, require:true, process:true*/
"use strict ";
//require library
var rl = require('readline-sync');
//require library
var rlasyncTemp = require('readline');
//creating interface to interact through console
var rlasync = rlasyncTemp.createInterface({
  input: process.stdin,
  output: process.stdout
});
var MYAPP25 = {};
MYAPP25.arithmetic = {
      add: function(a,b) {
        return a+b;
      },
      subtract: function(a,b) {
        return a-b;
      },
      multiply: function(a,b) {
        return a*b;
      },
      divide: function(a,b) {
        return a/b;
      }
    };
(function() {
  try {
    var isEmptyString = function(data) {
      if(data.trim()) {
        return false;
      }
      return true;
    };
    var op2;
    console.log("\n..Enter two values to perform arithmetic operations..(method invocation)\n");
    //read values and pass it..
    rlasync.question("Enter operand 1..  ",function(op1){
      try {
        //validate for empty string..
        if(isEmptyString(op1)) {
          throw "Invalid input..";
        }
        //convert string to numbers..
        op1 = Number(op1); 
        //validate the input adn proceed..
        if(isNaN(op1)) {
          throw "Invalid input..";
        }
        op2 = rl.question("Enter operand 2..  ");
        if(isEmptyString(op2)) {
          throw "Invalid input..";
        }
        //validate..
        if(isEmptyString(op2)) {
          throw "Invalid input..";  
        }
        //convert string to numbers..
        op2 = Number(op2);
        //validate the input adn proceed..
        if(isNaN(op2)) {
          throw "Invalid input..";
        }
        //pass values and display in console..
        console.log("Addition: "+MYAPP25.arithmetic.add(op1,op2));
        console.log("Subtraction: "+MYAPP25.arithmetic.subtract(op1,op2));
        console.log("Multiplication: "+MYAPP25.arithmetic.multiply(op1,op2));
        console.log("Division: "+MYAPP25.arithmetic.divide(op1,op2));
      } catch (er) {
        console.log(er);
        process.exit(0);
      }
    });
  } catch(e) {
    console.log("OOPS!! error occured..!! --> "+e);
    process.exit(0);
  }
})();
//////////////////////////////////////////////////////////////
module.exports = function() {
  return {
    arithmetic: MYAPP25.arithmetic
  };
};