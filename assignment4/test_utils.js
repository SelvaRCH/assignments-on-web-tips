/* jshint -W053*/
/* global console:true, describe:true, it:true, require:true, assert: true*/
var myUtils = require('./utilities.js');
var assert = require('assert');
(function() {
  var num = 9, str = 'selva', myObj = {'name': 'selva'}, bool = true, myArr = [1,2,3], test;
//inherit properties from Error  
  var errorConst = function(name) {
    this.name = name;
  };
  errorConst.prototype = new Error().prototype;
  //custom err obj
  var err  = new errorConst('test error');
  //create  a func
  var printName = function(obj) { 
    console.log(this.name);
  };
  describe('Functionality of Utilities.',function() {
    it('Testing Numbers',function() {
      assert.equal(false,myUtils.utilities.isArray(num));
      assert.equal(false,myUtils.utilities.isFunction(num));
      assert.equal(false,myUtils.utilities.isObject(num));
      assert.equal(false,myUtils.utilities.isBoolean(num));
      assert.equal(false,myUtils.utilities.isNan(num));
      assert.equal(false,myUtils.utilities.isError(num));
      assert.equal(false,myUtils.utilities.isNull(num));
      assert.equal(true,myUtils.utilities.isNumber(num));
      assert.equal(false,myUtils.utilities.isString(num));
      assert.equal(false,myUtils.utilities.isUndefined(num));
    });
    it('Testing Array',function() {
      assert.equal(true,myUtils.utilities.isArray(myArr));
      assert.equal(false,myUtils.utilities.isFunction(myArr));
      assert.equal(false,myUtils.utilities.isObject(myArr));
      assert.equal(false,myUtils.utilities.isBoolean(myArr));
      assert.equal(true,myUtils.utilities.isNan(myArr));
      assert.equal(false,myUtils.utilities.isError(myArr));
      assert.equal(false,myUtils.utilities.isNull(myArr));
      assert.equal(false,myUtils.utilities.isNumber(myArr));
      assert.equal(false,myUtils.utilities.isString(myArr));
      assert.equal(false,myUtils.utilities.isUndefined(myArr));
    });
    it('Testing Object',function() {
      assert.equal(false,myUtils.utilities.isArray(myObj));
      assert.equal(false,myUtils.utilities.isFunction(myObj));
      assert.equal(true,myUtils.utilities.isObject(myObj));
      assert.equal(false,myUtils.utilities.isBoolean(myObj));
      assert.equal(true ,myUtils.utilities.isNan(myObj));
      assert.equal(false,myUtils.utilities.isError(myObj));
      assert.equal(false,myUtils.utilities.isNull(myObj));
      assert.equal(false,myUtils.utilities.isNumber(myObj));
      assert.equal(false,myUtils.utilities.isString(myObj));
      assert.equal(false,myUtils.utilities.isUndefined(myObj));
    });
    it('Testing String',function() {
      assert.equal(false,myUtils.utilities.isArray(str));
      assert.equal(false,myUtils.utilities.isFunction(str));
      assert.equal(false,myUtils.utilities.isObject(str));
      assert.equal(false,myUtils.utilities.isBoolean(str));
      assert.equal(true ,myUtils.utilities.isNan(str));
      assert.equal(false,myUtils.utilities.isError(str));
      assert.equal(false,myUtils.utilities.isNull(str));
      assert.equal(false,myUtils.utilities.isNumber(str));
      assert.equal(true,myUtils.utilities.isString(str));
      assert.equal(false,myUtils.utilities.isUndefined(str));
    });
    it('Testing Function',function() {
      assert.equal(false,myUtils.utilities.isArray(printName));
      assert.equal(true,myUtils.utilities.isFunction(printName));
      assert.equal(false,myUtils.utilities.isObject(printName));
      assert.equal(false,myUtils.utilities.isBoolean(printName));
      assert.equal(true ,myUtils.utilities.isNan(printName));
      assert.equal(false,myUtils.utilities.isError(printName));
      assert.equal(false,myUtils.utilities.isNull(printName));
      assert.equal(false,myUtils.utilities.isNumber(printName));
      assert.equal(false,myUtils.utilities.isString(printName));
      assert.equal(false,myUtils.utilities.isUndefined(printName));
    });
    it('Testing for Boolean',function() {
      assert.equal(false,myUtils.utilities.isBoolean(num));
      assert.equal(false,myUtils.utilities.isBoolean(str));
      assert.equal(false,myUtils.utilities.isBoolean(printName));
      assert.equal(false,myUtils.utilities.isBoolean(myArr));
      assert.equal(false ,myUtils.utilities.isBoolean(myObj));
      assert.equal(true,myUtils.utilities.isBoolean(true));
      assert.equal(true,myUtils.utilities.isBoolean(new Boolean(undefined)));
      assert.equal(false,myUtils.utilities.isBoolean(12));
      assert.equal(false,myUtils.utilities.isBoolean(err));
      assert.equal(false,myUtils.utilities.isBoolean(undefined));
      assert.equal(false,myUtils.utilities.isBoolean(null));
    });
    it('Testing for null',function() {
      assert.equal(false,myUtils.utilities.isNull(num));
      assert.equal(false,myUtils.utilities.isNull(str));
      assert.equal(false,myUtils.utilities.isNull(printName));
      assert.equal(false,myUtils.utilities.isNull(myArr));
      assert.equal(false ,myUtils.utilities.isNull(myObj));
      assert.equal(false,myUtils.utilities.isNull(true));
      assert.equal(false,myUtils.utilities.isNull(new Boolean(undefined)));
      assert.equal(false,myUtils.utilities.isNull(12));
      assert.equal(false,myUtils.utilities.isNull(err));
      assert.equal(false,myUtils.utilities.isNull(undefined));
      assert.equal(true,myUtils.utilities.isNull(null));
    });
    it('Testing for undefined',function() {
      assert.equal(false,myUtils.utilities.isUndefined(num));
      assert.equal(false,myUtils.utilities.isUndefined(str));
      assert.equal(false,myUtils.utilities.isUndefined(printName));
      assert.equal(false,myUtils.utilities.isUndefined(myArr));
      assert.equal(false ,myUtils.utilities.isUndefined(myObj));
      assert.equal(false,myUtils.utilities.isUndefined(true));
      assert.equal(false,myUtils.utilities.isUndefined(new Boolean(undefined)));
      assert.equal(false,myUtils.utilities.isUndefined(12));
      assert.equal(false,myUtils.utilities.isUndefined(err));
      assert.equal(true,myUtils.utilities.isUndefined(undefined));
      assert.equal(false,myUtils.utilities.isUndefined(null));
    });
    it('Testing for Error',function() {
      assert.equal(false,myUtils.utilities.isError(num));
      assert.equal(false,myUtils.utilities.isError(str));
      assert.equal(false,myUtils.utilities.isError(printName));
      assert.equal(false,myUtils.utilities.isError(myArr));
      assert.equal(false ,myUtils.utilities.isError(myObj));
      assert.equal(false,myUtils.utilities.isError(true));
      assert.equal(false,myUtils.utilities.isError(new Boolean(undefined)));
      assert.equal(true,myUtils.utilities.isError(new Error('sample')));
      assert.equal(false,myUtils.utilities.isError(err));
      assert.equal(false,myUtils.utilities.isError(undefined));
      assert.equal(false,myUtils.utilities.isError(null));
    });
    it('Testing for isNan',function() {
      assert.equal(false,myUtils.utilities.isNan(num));
      assert.equal(true,myUtils.utilities.isNan(str));
      assert.equal(true,myUtils.utilities.isNan(printName));
      assert.equal(true,myUtils.utilities.isNan(myArr));
      assert.equal(true ,myUtils.utilities.isNan(myObj));
      assert.equal(true,myUtils.utilities.isNan(true));
      assert.equal(true,myUtils.utilities.isNan(new Boolean(undefined)));
      assert.equal(false,myUtils.utilities.isNan(12));
      assert.equal(true,myUtils.utilities.isNan(err));
      assert.equal(true,myUtils.utilities.isNan(undefined));
      assert.equal(true,myUtils.utilities.isNan(null));
    });
  });
})();
