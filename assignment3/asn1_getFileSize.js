/*global require:true, console:true, process:true*/
"use strict ";
try {
  //get fs module..
  var fs = require('fs');
  //get module for promises
  var q = require('q');
  //get readline for interfacing..
  var readline = require('readline');
  //create interface
  //creating interface to interact through console
  var rlasync = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  //////////////////////////////////////////////////////////////////////////
  //define function to get all file size
  var getAllFileSize = function (path) {
    var filePromise = q.defer();
    var totalSize = 0, count = 0, that = this;
    this.totalFiles = 0;
    function getFileSize(path,name) {
  //    console.log('reading file size');
      fs.stat(path+"//"+name,function(err,stats) {
        if(err) {
          console.log('err :'+err);
          throw  err;
        }
//        console.log("size of file "+name+" is "+stats.size);
        totalSize += stats.size;
        filePromise.notify('bytes added->'+stats.size);
        ++count;
        if(count === that.totalFiles) {
          console.log('size in bytes :'+totalSize);
//          return  totalSize;
          filePromise.resolve(totalSize);
        }
      });  
    }
    if(!path) {
      return 'invalid path';
    }
    //read files from given path
    fs.readdir(path,function(err,files) {
      if(err) {
        console.log('err :'+err);
//        return err;
        filePromise.reject(err);
      }
      //set the length of files
      this.totalFiles = files.length;
      //get file size of all files and add it.
      for(var file in files) {
  //      console.log(files[file]);
        getFileSize(path,files[file]);
      }
    });
    return filePromise.promise;
  };
  //get path and call the function..
  var path ; //'D://AFE4404//';
//  console.log(path);
  rlasync.question("\nPlease enter path to get total file size..  ",function(path){
    getAllFileSize(path).then(function(sucsCallback) {
      console.log('total File size: '+sucsCallback);
    },function(errCallBack) {
      throw errCallBack;
    },function(notification) {
      console.log('you have an update: '+notification);
    });
    rlasync.close();
  });
} catch(e) {
  console.log('err: '+e);
  process.exit(0);
}