/*global $:true, document:true*/
$(document).ready(function() {
//        $('.mainContent .accordian ul li .heading').addClass('active');
  //initially slide up all elements..
  $('.mainContent .accordian ul li').siblings().children('.desc').slideUp(0);
  //func..
  $('.mainContent .accordian ul li').find('.heading').on('click',function() {
    //toggle current heading
    $(this).toggleClass('active');
    //on click toggle the description
    $(this).siblings('.desc').slideToggle();
    //slide all other descriptions up
    $(this).parent('li').siblings().children('.desc').slideUp();
    //remove active class from prev selected heading
    $(this).parents('li').siblings().children().removeClass('.active');
  });
});