* Assignment Questions :

1_a : Print a diamond shape in console using '*'. No. of lines should be configurable

1_b : Create a Fibonacci. Series length should be configurable

1_c : Reverse a string after removing the vowels and spaces from the given sentence

1_d : Create an array of student objects. Each object should contain id, age, name etc. Sort based on the id

1_e : Create a Object to contain authors and their works. Have the parameter to represent author name and the book names as part of values. Create functions to add new books, new author and search based on book names (partial field is also expected)