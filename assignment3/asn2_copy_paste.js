/*global require:true, console:true, process:true*/
"use strict ";
//get fs module..
var fs = require('fs');
//////////////////////////////////////////////////////////////////////////
try {
  var copyPaste = function() {
    //verfify the existence of folder and proceed
    function copyToNewFile(src,dest) {
//      console.log(dest);
      var dirPath = dest.substr(0,dest.lastIndexOf('\\'));
      console.log('dirPath: '+dirPath);
      fs.exists(dirPath,function(callback)  {
        console.log('existnce: '+callback);
        //if directory exists then just write to the destination
        if(callback) {
          startCopy(src,dest);
        } else {
          //else create a folder and then do copy
          fs.mkdir(dirPath+"\\",function(err){
            if(err) {
              console.log('failed to create directory: '+err);
            }
          startCopy(src,dest);
          });
        }
      });
      var startCopy = function(src,dest) {
        fs.readFile(src,function(err,data) {
  //        console.log('file-read :'+src+'\ndata: '+data);
          //write the data read to destination
          fs.writeFile(dest,data,function(err) {
            if(err) {
              console.log('err: '+err);
              throw err;
            }
            console.log('data written successfully from src to a dest');
            return;
          });
          return;
        });
      };
    }
    var configLoc = './config.json';
    //read the config file and start the process..
    fs.readFile(configLoc,'utf-8',function(err,data) {
      if(err) {
//        console.log('err :'+err);
        throw err;
      }
      //convert to array format
      data = JSON.parse(data);
      if(!(Array.isArray(data) && data.length)) {
        console.log('NOT A VALID JSON');
        throw 'NOT A VALID JSON';
      }
      //display the data
//      console.log(data);
      //run a loop open the specified file, and write to destination
      for(var el=0; el<data.length; ++el) {
//        console.log(data[el].source,data[el].destination);
        copyToNewFile(data[el].source,data[el].destination);
      }
    });
  };
  //start the process..
  copyPaste();
} catch(e) {
  console.log('err :'+e);
}