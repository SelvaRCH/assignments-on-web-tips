/*global require:true, console:true, process:true*/
"use strict ";
//get fs module..
var fs = require('fs');
//////////////////////////////////////////////////////////////////////////
try {
  var configLoc = './config.json';
  //necessary var to find time req
  var timeTracker = [], fileLength;
  //function to write the measured time in config.json
  var writeMeasuredTime = function() {
    //read the config file..
    fs.readFile(configLoc,function(err,configData) {
      if(err) {
        console.log('time could not be updated in config file');
        throw err;
      }
      //convert to array format
      configData = JSON.parse(configData);
      if(!(Array.isArray(configData) && configData.length)) {
        console.log('config file changed!!');
        throw 'config file changed!!';
      }
      //write the timings recorded
      for(var i=0; i<timeTracker.length; ++i) {
        if(timeTracker[i].hasOwnProperty('dateEnd')) {
          configData[i].timeRequired = timeTracker[i].dateEnd;
          console.log(i+' '+configData[i].timeRequired+'ms');
        }
      }
      //now write to the file
//      console.log(config);
      fs.writeFile(configLoc,JSON.stringify(configData),function(err){
//        console.log('config file updated  ');
        if(err) {
          console.log('err while updating config file..');
          throw err;
        }
      });
    });
  };
  //where the execution starts
  var copyPaste = function() {
    //verfify the existence of folder and proceed
    //performs validating the path and invoking func to do copy & paste
    function copyToNewFile(src,dest,el) {
      var dirPath = dest.substr(0,dest.lastIndexOf('\\'));
//      console.log('dirPath: '+dirPath);
      fs.exists(dirPath,function(callback)  {
//        console.log('existnce: '+callback);
        //if directory exists then just write to the destination
        if(callback) {
          startCopy(src,dest,el);
        } else {
          //else create a folder and then do copy
          fs.mkdir(dirPath+"\\",function(err){
            if(err) {
              console.log('failed to create directory: '+err);
            }
          startCopy(src,dest,el);
          });
        }
      });
      //core func  that performs copy_paste
      var startCopy = function(src,dest,el) {
        fs.readFile(src,function(err,data) {
  //        console.log('file-read :'+src+'\ndata: '+data);
          if(err) {
            console.log('err occured in file read: ',err);
            throw err;
          }
          //write the data read to destination
          fs.writeFile(dest,data,function(err) {
            if(err) {
              console.log('err: '+err);
              throw err;
            }
            //get time2, and find the time required to copy each file
            timeTracker[el].dateEnd = (new Date().getTime()) - (timeTracker[el].dateStart);            
//            console.log(el+' '+timeTracker[el].dateEnd);
//            console.log('data written successfully from src to a dest');
            //once all file is done call func to mark recorded timings
            if(el+1 === fileLength) { 
              //now call a func to write the time in config.json
              writeMeasuredTime();
            }
            return;
          });
          return;
        });
      };
    }
    //read the config file and start the process..
    fs.readFile(configLoc,'utf-8',function(err,data) {
      if(err) {
//        console.log('err :'+err);
        throw err;
      }
      //convert to array format
      data = JSON.parse(data);
      if(!(Array.isArray(data) && data.length)) {
        console.log('NOT A VALID JSON');
        throw 'NOT A VALID JSON';
      }
      fileLength = data.length;
      //display the data
//      console.log(data);
      //run a loop open the specified file, and write to destination
      for(var el=0; el<data.length; ++el) {
//        console.log(data[el].source,data[el].destination);
        //get time1, befre reading src file
        timeTracker[el] = {};
        timeTracker[el].dateStart = new Date().getTime();
        copyToNewFile(data[el].source,data[el].destination,el);
      }
    });
  };
  //start the process..
  copyPaste();
} catch(e) {
  console.log('err :'+e);
} finally {
  //write the timimgs recorded
//  writeMeasuredTime();  
}