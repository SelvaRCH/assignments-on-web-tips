/*global console:true, module:true*/
"use strict ";
var MYAPP23 = {};
MYAPP23.enumCam = function(camera) {
  var resultStr = '';
  console.log("\nDisplaying properties availbale in object Camera..,\n");
  if(!(typeof camera === 'object' && Object.keys(camera).length)) {
    return false;
  }
  for(var prop in camera) {
      //iterate over prop of current obj, and not its prototypes
      if(camera.hasOwnProperty(prop)) {
        resultStr += (prop+": "+camera[prop]+"\n");
      }
    }
  return resultStr;
};
 //enumerate and display camera obj
  MYAPP23.enumCamTypeNumber = function(camera) {
    var resultStr = '';
    console.log("\nDisplaying properties that of type 'Number', availbale in object Camera..,\n");
    if(!(typeof camera === 'object' && Object.keys(camera).length)) {
      return false;
    }
    for(var prop in camera) {
      //iterate over prop of current obj, and not its prototypes
      //also check for type number
      if(camera.hasOwnProperty(prop) && typeof camera[prop] === 'number') {
        resultStr += (prop+": "+camera[prop]+"\n");
      }
    }
    if(!resultStr.length) {
      return false;
    }
    return resultStr;
  };
var initiateProcess = function() {
  //define proprties for camera.
  var camera = {
    "focalLengthIn(mm)": 2.4,
    "lensDiaIn(mm)": 30,
    "totalStorageUnit": 2000,
    "unitsOccupied": 2,
    "resolutionIn(MP)": 5,
    "weight": "150 gm",
    "yearReleased": 2,
    "color": "Black",
    "price": 50000,
    "brand": "SONY",
    "series": "DSLR E 45",
    "picStorage": ['pic1','pic2'],
    takePic: function() {
      ++this.unitsOccupied;
      this.picStorage.push("pic"+this.unitsOccupied);
      console.log("The amazing moment is captured lively..");
    },
    wipeStorage: function() {
      this.picStorage = [];
      console.log("Storage is cleaned");
    },
    deleteSelectedPic: function(picId) {
      delete this.picStorage[this.storage.indexOf(picId)];
      console.log("picture "+picId+" is deleted");
    }
  };
  //display prop of enum camera
  console.log(MYAPP23.enumCam(camera));
  //display prop of type 'Number'
  console.log(MYAPP23.enumCamTypeNumber(camera));
};
//start  the program
initiateProcess();
//////////////////////////////////////////////////////////////
module.exports = function() {
  return {
    enumCam: MYAPP23.enumCam,
    enumCamTypeNumber: MYAPP23.enumCamTypeNumber
    };
};