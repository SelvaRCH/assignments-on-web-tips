/*global console:true, module:true, require:true, process:true*/
"use strict ";
(function() {
  try {
    //require dependencies
    var rl = require('readline-sync');
    var isEmptyString = function(data) {
      if(data.trim()) {
        return false;
      }
      return true;
    };
    //create a arithmeticConstructor
    var arithmeticConstructor = function(a,b) {
      this.a = a;
      this.b = b;
    };
    //create methods for arithmetic 
    arithmeticConstructor.prototype.add = function() {
      console.log("add: "+(this.a+this.b));
    };
    arithmeticConstructor.prototype.subtract = function() {
      console.log("subtract: "+(this.a-this.b));
    };
    arithmeticConstructor.prototype.multiply = function() {
      console.log("multiply: "+(this.a*this.b));
    };
    arithmeticConstructor.prototype.divide = function() {
      console.log("divide: "+(this.a/this.b));
    };
    //declare storage for variables
    var op1,op2;
    console.log("\n..Enter two values to perform arithmetic operations..(constructor invocation)\n");
    //read values and pass it..
    op1 = rl.question("Enter operand 1..  ");
    //validate for empty string..
    if(isEmptyString(op1)) {
      throw "Invalid input..";
    }
    //convert string to numbers..
    op1 = Number(op1); 
    //validate the input adn proceed..
    if(isNaN(op1)) {
      throw "Invalid input..";
    }
    op2 = rl.question("Enter operand 2..  ");
    if(isEmptyString(op2)) {
      throw "Invalid input..";
    }
    //validate..
    if(isEmptyString(op2)) {
      throw "Invalid input..";  
    }
    //convert string to numbers..
    op2 = Number(op2);
    //validate the input adn proceed..
    if(isNaN(op2)) {
      throw "Invalid input..";
    }
    //if both the inputs are valid, then create a new objects and call required methods..
    var arithmetic = new arithmeticConstructor(op1,op2);
    arithmetic.add(); 
    arithmetic.subtract(); 
    arithmetic.multiply(); 
    arithmetic.divide();
  } catch(e) {
    console.log("OOPS!! error occured..!! --> "+e);
  }
})();