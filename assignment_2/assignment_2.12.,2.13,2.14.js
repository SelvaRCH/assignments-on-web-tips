/*global console:true, module:true, require:true, process:true*/
"use strict ";
//adding method to string prototype..
String.prototype.findVowel = function() {
  var constraints = ['a','e','i','o','u','A','E','I','O','U'];
  var temp = '';
  var str = this.valueOf();
  for(var i=0; i<str.length; ++i) {
    if(constraints.indexOf(str[i]) !== -1) {
      temp += ("Index: "+i+"  Vowel :"+str[i]+"  \n");
    }
  }
  if(!temp) {
    return false;
  }
  return temp;
};
//adding method to number prototype..
Number.prototype.fact = function() {
  var num = this.valueOf();
  if(isNaN(num) || num<=0) {
    return undefined;
  }
  var result = 1;
  for(var i=2; i<=num; ++i){
    result *= i;
  }
  return result;
};
//require node-modules
var rl = require('readline-sync');
//require library
var rlasyncTemp = require('readline');
//creating interface to interact through console
var rlasync = rlasyncTemp.createInterface({
  input: process.stdin,
  output: process.stdout
});
var MYAPP212 = {};
(function(){
  try {
    var myInterest = [
        'Cycling',
        'Programming in C',
        'Riding Bikes',
        'Playing Carrom',
        'Listening Music',
        'Reading Short Stories',
        'Learning new Languages'
      ];
    //set interested things in an array and display
    MYAPP212.displayInterested = function(myInterest) {
      var str = '';
      if(Array.isArray(myInterest) && myInterest.length) {
        for(var el in myInterest) {
          str += (myInterest[el]+"\n");
        }
        return str;
      }
      return false;
    };
    //start the process
    var provideOptions = "\n(Adding methods to number and string prototypes..)\n";
    provideOptions += "\nChoose optionfrom listed..\n1.Find vowel of a String..\n";
    provideOptions += "2.Find factorial of a number\n3.View the array of interested things";
    provideOptions += "\nPlease enter your choice here..  ";
    rlasync.question(provideOptions,function(choice){
    switch(choice) {
      case '1':
        var myStr = rl.question("Please enter a string to find all Vowels..\n");
        console.log(myStr.findVowel());
        break;
      case '2':
        var myNumber = Number(rl.question("Please enter a number to find its factorial.. "));
        console.log(myNumber.fact());
        break;
      case '3':
        console.log("\n");
        console.log(MYAPP212.displayInterested(myInterest));
        break;
      default: console.log("Invalid Choice.. Exiting");
    }
    });
  } catch(e) {
    console.log("OOPS!! Error Occurred -> "+e);
  }
})();
//////////////////////////////////////////////////////////////
module.exports = function() {
  return {
    displayInterested: MYAPP212.displayInterested
  };
};