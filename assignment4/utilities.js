/* global module:true*/
"use strict ";
var utils = {};
utils.isUndefined = function(obj) {
  if(!obj && isNaN(Number(obj))) {
    return true;
  }
  return false;
};
utils.isNull = function(obj) {
  if(!obj && obj !== undefined) {
    return true;
  }
  return false;
};
utils.isNan = function(obj) {
  if(obj && obj.constructor !== Number) {
    return true;
  }
  if(!obj) {
    return true;
  }
  return false;
};
utils.isError = function(obj) {
  if(obj && obj instanceof Error) {
    return true;
  }
  return false;
};
utils.isBoolean = function(obj) {
  if(obj && obj.constructor === Boolean) {
    return true;
  }
  return false;
};
utils.isNumber = function(obj) {
  if(obj && obj.constructor === Number) {
    return true;
  }
  return false;
};
utils.isString = function(obj) {
  if(obj && obj.constructor === String) {
    return true;
  }
  return false;
};
utils.isFunction = function(obj) {
 if(obj && obj.constructor === Function) {
   return true;
 }
 return false;
};
utils.isObject = function(obj) {
  if(obj && obj.constructor === Object) {
    return true;
  }
  return false;
};
utils.isArray = function(obj) {
  if(obj && obj.constructor === Array) {
    return true;
  }
  return false;
};
module.exports = {
    utilities : utils
};